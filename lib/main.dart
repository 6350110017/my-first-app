import 'package:flutter/material.dart';

void main() {
  runApp(const MyFirsApp());
}

class MyFirsApp extends StatelessWidget {
  const MyFirsApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "PSU Trang",
      home: Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.insert_emoticon),
          title: Text(
            'My First App',
            style: TextStyle(fontSize: 30, color: Colors.lightBlue),
          ),
          backgroundColor: Colors.red,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.access_time_outlined),
              tooltip: 'Show Hello Wolrd',
              onPressed: () {
                ScaffoldMessenger.of(context)
                    .showSnackBar(SnackBar(content: Text('Hello World')));
              },
            ),
            IconButton(
              onPressed: () {},
              tooltip: 'Search',
              icon: Icon(Icons.search),
            ),
          ],
        ),
        backgroundColor: Colors.redAccent,
        body: Column(
          children:const [
            CircleAvatar(
              backgroundColor: Colors.black,
              radius: 200,

              child: CircleAvatar(
                  radius: 190,
                  backgroundImage: AssetImage(
                    'assets/name.jpg',)),
            ),

            Text('นายศรายุธ สิทธิเดช',
                style: TextStyle(height: 3, fontSize: 30)),
            Text('6350110017', style: TextStyle(height: 1, fontSize: 15)),

          ],

        ),
      ),
    );
  }
}
